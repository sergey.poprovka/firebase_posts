let Vue = require('vue')

const firebase = require('firebase')
const firebaseui = require('firebaseui')

let firebaseConfig = {
    apiKey: "AIzaSyB2-N88yB-b7h-laYMWy3xVUxa6lHo0Qbo",
    authDomain: "sergiipoprovka.firebaseapp.com",
    databaseURL: "https://sergiipoprovka.firebaseio.com",
    projectId: "sergiipoprovka",
    storageBucket: "sergiipoprovka.appspot.com",
    messagingSenderId: "125722690093",
    appId: "1:125722690093:web:9a85ef78f487ed8f72eea4"
};
firebase.initializeApp(firebaseConfig);

export default firebase

Vue.component('app', require('./components/App').default)
Vue.component('posts', require('./components/Posts').default)
Vue.component('single-post', require('./components/SinglePost').default)
Vue.component('create-post', require('./components/CreatePost').default)

let app = new Vue({
    el:"#app"
})